import bs4 as bs
import pickle
import requests

def save_sp500_tickers(output_dir="./"):
    """
    Scraps wikipedia page to get the full updated sp500 list. Taken from Sendtex tutorial.
    :param output_dir: directory that will contain the output file.
    
    :return list() tickers: list containing the 505 abbreviations of the organizations in sp500.
    """
    resp = requests.get("https://en.wikipedia.org/wiki/List_of_S%26P_500_companies")
    soup = bs.BeautifulSoup(resp.text, 'lxml')
    table = soup.find('table', {'class': 'wikitable sortable'})
    tickers = []
    
    for row in table.findAll('tr')[1:]:
        ticker = row.findAll('td')[0].text
        tickers.append(ticker)
        
    with open(output_dir+"sp500tickers.pickle", "wb") as f:
        pickle.dump(tickers, f)
        
        
    return tickers

if __name__ == '__main__':
    save_sp500_tickers()