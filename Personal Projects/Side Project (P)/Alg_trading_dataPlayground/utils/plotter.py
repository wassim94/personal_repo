import plotly
import plotly.plotly as py
import plotly.offline as offline
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from plotly.graph_objs import Scatter, Figure, Layout

import numpy as np


def get_plot_data(year_start, year_end, data, num_stocks, plot_feature):
    """
    assembles given data according to a to-be-plotted column for randomly picked stocks.
    :param int year_start, year_end: years where data is considered.
    :param pd.DataFrame() data: pandas dataframe containing the data to be plotted later.
    :param int num_stocks: number of stocks to be picked randomly.
    :param str() plot_feature: string containing the column names for which the different stocks will be assembled
    
    :return pd.DataFrame() assembled_data: pandas dataframe containing the dates and a column for each 
    stock containing the chosen feature to be plotted.
    """
    
    
    #init output
    assembled_data = []
    
    #assert num stocks doesnt exceed data length
    assert len(data)>= num_stocks, "num stocks is too large dude."
    
    #get stock names
    stock_names = list(data.keys())
    print("list of stocks: ", stock_names)
    
    #pick distinct num_stocks number of random stocks
    stock_names = np.random.choice(stock_names, num_stocks, replace=False)
    
    for stock_name in stock_names:
        #extract stock
        stock = data[stock_name]
                
        #filter stock
        asset = stock[(stock["Date"].dt.year >= year_start) & (stock["Date"].dt.year <= year_end)]
        
        #print(asset["Date"].dt.strftime(date_format='%Y-%m-%d').values)
        #print(asset[plot_feature].values)
        assembled_data.append(go.Bar(
                                    x = asset["Date"].dt.strftime(date_format='%Y-%m-%d').values,
                                    y = asset[plot_feature].values,
                                    name = stock_name
                                    )
                             )
        
    return assembled_data
    

def plot_trend(year_start, year_end, data, num_stocks, plot_feature):
    """
    given certain stock data, plot it using plotly over a given period of time
    :param int year_start, year_end: years where data is considered.
    :param pd.DataFrame() data: pandas dataframe containing the data to be plotted later.
    :param int num_stocks: number of stocks to be picked randomly.
    :param str() plot_feature: string containing the column names for which the different stocks will be assembled
    
    :return None: plots the given data using plotly.
    """    
    layout = go.Layout(dict(title = plot_feature + " Price of Random " + str(num_stocks) + " assests overall for " + str(year_start) + "-" + str(year_end),
                            xaxis = dict(title = "Month"),
                            yaxis = dict(title = "Price (USD)"),
                           ),
                       legend = dict(orientation = 'h'))
    iplot(dict(data = data, layout=layout), filename = 'basic-line')