from quandl import get
from pandas import to_datetime, DataFrame
import os
import pandas_datareader.data as web
from collections import Counter
import numpy as np


def mr_inspect(df):
    """
    Returns an inspection dataframe. Took from a tutorial. Inspects NANs, unique values and NONEs
    :param pd.DataFrame(): dataframe to be inspected.
    
    :return pd.DataFrame() inspect_dataframe: df containing the above mentioned information.
    """
    print ("Length of dataframe:", len(df))
    inspect_dataframe = DataFrame({'dtype': df.dtypes, 'Unique values': df.nunique() ,
                 'Number of missing values': df.isnull().sum() ,
                  'Percentage missing': (df.isnull().sum() / len(df)) * 100
                 }).sort_values(by='Number of missing values', ascending = False)
    return inspect_dataframe        
        
def oc_split_numpy(data, columns, data_type="numpy", name="Stock"):
    '''
    returns the queried columns from the tabular data independently from its format
    :param data: output of quandl.get
    :param str columns: which columns are to be extracted by name
    :param str data_type: data type of "data" - numpy or pandas
    
    :returns: dict() with {"column": data}. Date is always returned as pd.Timestamp, except if columns==[]
    '''
    splitted_data = {}    
    
    if columns == []:
        return data
    
    for column in columns:
        splitted_data[column] = data[column]
        if column == "Date":
            splitted_data[column] = to_datetime(splitted_data[column])
              
    return splitted_data

def oc_split_pandas(data, columns, data_type="numpy", name="Stock"):
    '''
    returns the queried columns from the tabular data independently from its format
    :param data: output of quandl.get
    :param str columns: which columns are to be extracted by name
    :param str data_type: data type of "data" - numpy or pandas
    
    :returns: dict() with {"column": data}. Date is always returned as pd.Timestamp, except if columns==[]
    '''
    splitted_data = {}
    
    print("Inspection of stock:", name)    
    
    print(data.head())

    print(mr_inspect(data))
    print("*"*30)
    
    
    if columns == []:
        return data
    
    for column in columns:
        if data_type == "pandas":
            if column == "Date":
                splitted_data[column] = data.axes[0]
            else:
                splitted_data[column] = data[column].values
                
    splitted_data = DataFrame.from_dict(splitted_data)
              
    return splitted_data



def get_data(datasets, apikey, start_date, end_date, data_type, columns=[]):
    '''
    returns the datasets inquired
    :param [str] data: datasets to be downloaded
    :param str apikey: quandl api key
    :param str start_date, end_date: e.g. "2018-08-20"
    :param str data_type: numpy of pandas
    :param [str] column: list of columns to be retrieved. [] downloads the whole dataset
    
    :returns: dict() with {"dataset": splitted_data} 
    '''
    data = dict()
    if data_type == "numpy":
        for dataset in datasets:
            tmp = get(dataset=dataset, api_key=apikey, start_date=start_date, end_date=end_date, returns=data_type)
            data[dataset] = oc_split_numpy(tmp, columns, data_type, name=dataset)
    else:
        for dataset in datasets:
            tmp = get(dataset=dataset, api_key=apikey, start_date=start_date, end_date=end_date, returns=data_type)
            data[dataset] = oc_split_pandas(tmp, columns, data_type, name=dataset)
        
    return data

def print_stocks(data):
    """
    print stock names
    """
    for stock in data.keys():
        print("Stock: ", stock)

def get_data_from_yahoo(tickers, start, end, data_dir):
    """
    given a list of tickers, this function imports the correpsonding data. Data is streamed from host and saved locally.
    :param list() tickers: list of tickers we want to process
    :param dt.datetime() start, end: datetime objects specifying the start-end dates of data to be imported
    
    :return None: save the dataa locally
    """
    
    if not os.path.exists(data_dir + 'stock_dfs'):
        os.makedirs(data_dir + 'stock_dfs')
        
    for ticker in tickers:
        if not os.path.exists(data_dir + 'stock_dfs/{}.csv'.format(ticker)):
            print("Pulling data for {}.....".format(ticker))
            df = web.DataReader(ticker, 'yahoo', start, end)
            df.to_csv(data_dir + 'stock_dfs/{}.csv'.format(ticker))
        else:
            print('Already have {}'.format(ticker))
            
def extract_featuresets(ticker, tickers, df, fn):
    """
    for every row, create a list()-column containing the actions that should made according to the function given. Taken from Sentdex tutorial.
    :param str() ticker: str containing the chosen ticker to calculate the actions within the upcoming 7 days.
    :param list() tickers: column names of the dataframe
    :param pd.DataFrame() df: dataframe containing the data
    :param fn(): functions that maps data to a dicrete room of actions (in our case to 0,1,-1)
    
    :return numpy.array() X: train data for machine learinng models. Contains pct.change data of the df
    :return list() y: label array for the X-training data
    :return df: dataframe containing the transformed data.    
    """
    df['{}_target'.format(ticker)] = list(map(fn,
                                              df['{}_1d'.format(ticker)],
                                              df['{}_2d'.format(ticker)],
                                              df['{}_3d'.format(ticker)],
                                              df['{}_4d'.format(ticker)],
                                              df['{}_5d'.format(ticker)],
                                              df['{}_6d'.format(ticker)],
                                              df['{}_7d'.format(ticker)]
                                             ))
    vals = df['{}_target'.format(ticker)].values.tolist()
    str_vals = [str(i) for i in vals]
    
    print('Data spread:', Counter(str_vals))

    df.fillna(0, inplace=True)
    
    #replace infinite changes in stock prices. This means something weird happened
    df = df.replace([np.inf, -np.inf], np.nan) 
    df.dropna(inplace=True)
    
    df_vals = df[[ticker for ticker in tickers]].pct_change()
    df_vals = df_vals.replace([np.inf, -np.inf], 0)
    df.fillna(0, inplace=True)
    
    #feature and label:
    X = df_vals.values
    y = df['{}_target'.format(ticker)].values
    
    return X, y, df