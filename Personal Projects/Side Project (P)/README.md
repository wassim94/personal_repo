This is a personal side project with **3 friends of mine**. 

We are still in the team building phase through through a pilot project: **algorithmic trading using Reinforcement Learning**.

Sadly I can only share some parts of my work here that are mainly in the **data pipeline module**.

This module is a work-in-progress, combining some of my own data import and preprocessing methods but also a handful of tutorials in the sake of 
having a greater insight to a field very unclear to us: financial and stock markets.

****

**What I am trying to solve** through this module is to:
- modulize our input methods for later replication with new data and new techniques.  
- gain insights as to what hand-crafted features traders use and what they stand for.

****

**timeline**:
```
the pilot project is planned to last on a window of **three months** after which we should have a working prototype of an
RL-agent that **makes profit**, not necesseraly to make super rich. It's aim is our team building.
```

****
**Shared Code:**
```
Since I am still in the exploratory part of the project, the most important work I'm doing is on jupyter
notebooks. Help-functions are to be found under ./utils/
```
```
I am currently working on building a data import and preprocessing pipeline from **quandl**.
**UPDATE:** the plan has changed, following Sendtex tutorial for machine learning for financial markets. Data Pipeline is taking
trading data directly from Yahoo.
```