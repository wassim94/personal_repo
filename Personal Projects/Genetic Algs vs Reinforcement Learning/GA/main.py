import gym
import numpy as np
import random

env = gym.make('FrozenLake-v0')

N_POPS = 100
N_EPISODES = 20
N_ITERATIONS = 100
EPISODE_MAX_LEN = 100

def run_pop_policy(pop_policy, env, n_iterations = N_ITERATIONS):
    total_reward = 0.0
    for i in np.arange(n_iterations):
        obs = env.reset()
        tmp_reward = 0.0
        for j in np.arange(EPISODE_MAX_LEN):
            obs, reward, done, _ = env.step(pop_policy[obs])
            tmp_reward += reward
            if done:
                break
        total_reward += tmp_reward# / EPISODE_MAX_LEN
    return total_reward / n_iterations

def crossover(pop1, pop2):
    pop_idx = np.random.randint(len(pop1))
    new_pop = np.where(pop_idx, pop1, pop2)
    return new_pop

def mutate(pop, action_space_size, rate=0.05):
    rand_results = np.random.rand(len(pop))
    rand_pop = np.random.randint(action_space_size, size=[len(pop)])
    pop = np.where(rand_results > rate, pop, rand_pop)
    return pop

if __name__ == '__main__':
    pops = np.random.randint(env.action_space.n, size=[N_POPS, env.observation_space.n])
    for i_episode in range(N_EPISODES):
        pop_rewards = np.zeros(shape=[N_POPS])
        # Run environment for pop
        for i in np.arange(N_POPS):
            obs = env.reset()
            pop = pops[i]
            reward = run_pop_policy(pop, env)
            pop_rewards[i] = reward
        print("Average reward in episode {}: {:0.5f}".format(i_episode , np.sum(pop_rewards) / N_POPS))
        print("Best reward in episode {}: {:0.5f}".format(i_episode, np.max(pop_rewards)))

        mutated_pops = np.zeros(shape=pops.shape)

        best_pops_idx = np.argsort(pop_rewards)[-5:]

        # Crossover & mutate selected pops
        for i in np.arange(N_POPS - 5):
            crossover_pop_idx = np.random.choice(pops.shape[0], 2, p=pop_rewards/np.sum(pop_rewards))
            crossover_pops = pops[crossover_pop_idx]
            new_pop = crossover(crossover_pops[0], crossover_pops[1])
            new_pop = mutate(new_pop, env.action_space.n)
            mutated_pops[i] = new_pop
        mutated_pops[-5:] = pops[best_pops_idx]
        pops = mutated_pops