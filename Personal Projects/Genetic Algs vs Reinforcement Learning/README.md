# Project Description
This project is a TUM student project that aims to exhibit our learning from the course "Introduction to Deep Learning."
We chose to make a comparison between **Reinforcement Learning** and **Genetic Algorithms** since these were both very interesting to our team
and they show some resemblence in the concept of **imitating** human thinking and population development. 

# Team
We were a team of 4 people

# My Part
Together with one teammate I worked on the Reinforcement Learning part developing two agents for 4x4 and 8x8 frozen lake problem.
As long as I remember we tried to deploy an agent over the Bipolar walker but due to timeline limits we failed in getting it to actually
learn and adapt.

# Project Summary
We had to create a poster for the final presentation that explains the whole project and the gained insights. It was printed on an A3-paper
and therefore you might have to zoom in a little to see the different plots.
