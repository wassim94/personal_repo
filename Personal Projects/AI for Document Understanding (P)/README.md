I sadly cannot share any of my code in this project. But after discussing this with the project stakeholders, this is what I can share:

****
**Project Description:**
- The project is a joint project between the TUM and a consulting company under the TUM Data Innovation Lab.
- The aim of the project is to implement a whole pipeline as follows:
```
1. Input scanned document.
2. Classify the input (e.g. letter, email etc.)
3. Perform Optical Character Recognition (OCR) on the input to transform the image to string.
4. Perform Named Entity Recognition on the output of the last step.
5. Index the output of the last step and post it to Solr Server (Search engine).
6. Connect the Solr server to Banana (visualization platform for search engines).
```

- Team:
```
We are a team of 4 Math students at the TUM. 
```

- Timeline:
```
The whole pipeline was planned to last two semesters. Lately we had a discussion and agreed that we can 
finish the whole pipeline this semester, i.e. from September -> February. 
```
***
**Organisation:**
I am working as the Scrum master in this project. Our client are both the university and Capgemini.

**Coding:**
- I was envolved in the following coding modules:
  * Document Image Classification using transfer learning, multiple CNNs and an Ensembler.
  * OCR using pyTesseract and OCR.Space
  * Partly in the NER module as a support.
  * Solr module


The project does not belong to us and it is not a private project.