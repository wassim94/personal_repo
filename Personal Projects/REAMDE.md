# Personal Projects:

In this directory, you will find projects that I have worked on and ones I am still working on.

**Project with (P) at the end** are projects where I am either not allowed to share the whole code or parts of it. 

**BUT** you will at least find a readme file explaining my contribution and what the project is about.  


***
1. AI for Document Understand (P):
    - Joint project of the university and a consulting company. **No code** is shared but an explanatory REAMDE is to be found there.
2. Genetic Algs vs Reinforcement Learning:
    - University project. You will find the integrity of the project in the directory. 
    - This project was done 1.5 years ago. I was messy (not messi, sadly), but I think I learned my way out of it. And I am pretty confident I can write clean
(and very well commented) code now.
3. Side Project (P):
    - three weeks ago, I started working on a side project with some friends. For now it includes a stock trading (Deep) Reinforcement Learning agent.
    Nevertheless, we are not bound to it. 
    - Some code will be found there. Not all of it, due to obvious reasons.