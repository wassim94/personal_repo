
# coding: utf-8

# # This is an example of a two-level coarsening using METIS algorithm - mainly the first part
# ## 1) Import

# In[1]:


import numpy as np
import scipy.sparse
import scipy.sparse as scp


# ## 2) Input

# In[2]:


levels = 1
self_connections = False
adjacency_matrix = np.array([
    [0,1,0,0,1],
    [1,0,1,0,1],
    [0,1,0,1,0],
    [0,0,1,0,1],
    [1,1,0,1,0]
])
sparse_adj = scp.csr_matrix(adjacency_matrix)


# ## 3) METIS coarsening algorithm:
# ### 3.1) Clustering per level

# In[3]:


def metis(W, levels, rid=None):
    """
    Coarsen a graph multiple times using the METIS algorithm.

    INPUT
    W: symmetric sparse weight (adjacency) matrix
    levels: the number of coarsened graphs

    OUTPUT
    graph[0]: original graph of size N_1
    graph[2]: coarser graph of size N_2 < N_1
    graph[levels]: coarsest graph of Size N_levels < ... < N_2 < N_1
    parents[i] is a vector of size N_i with entries ranging from 1 to N_{i+1}
        which indicate the parents in the coarser graph[i+1]
    nd_sz{i} is a vector of size N_i that contains the size of the supernode in the graph{i}

    NOTE
    if "graph" is a list of length k, then "parents" will be a list of length k-1
    """

    N, N = W.shape
    if rid is None:
        rid = np.random.permutation(range(N))
    parents = []
    degree = W.sum(axis=0) - W.diagonal()
    graphs = []
    graphs.append(W)
    #supernode_size = np.ones(N)
    #nd_sz = [supernode_size]
    #count = 0

    #while N > maxsize:
    for _ in range(levels):

        #count += 1

        # CHOOSE THE WEIGHTS FOR THE PAIRING
        # weights = ones(N,1)       # metis weights
        weights = degree            # graclus weights
        # weights = supernode_size  # other possibility
        weights = np.array(weights).squeeze()

        # PAIR THE VERTICES AND CONSTRUCT THE ROOT VECTOR
        idx_row, idx_col, val = scipy.sparse.find(W)
        perm = np.argsort(idx_row)
        rr = idx_row[perm]
        cc = idx_col[perm]
        vv = val[perm]
        cluster_id = metis_one_level(rr,cc,vv,rid,weights)  # rr is ordered
        parents.append(cluster_id)

        # TO DO
        # COMPUTE THE SIZE OF THE SUPERNODES AND THEIR DEGREE 
        #supernode_size = full(   sparse(cluster_id,  ones(N,1) , supernode_size )     )
        #print(cluster_id)
        #print(supernode_size)
        #nd_sz{count+1}=supernode_size;

        # COMPUTE THE EDGES WEIGHTS FOR THE NEW GRAPH
        nrr = cluster_id[rr]
        ncc = cluster_id[cc]
        nvv = vv
        Nnew = cluster_id.max() + 1
        # CSR is more appropriate: row,val pairs appear multiple times
        W = scipy.sparse.csr_matrix((nvv,(nrr,ncc)), shape=(Nnew,Nnew))
        W.eliminate_zeros()
        # Add new graph to the list of all coarsened graphs
        graphs.append(W)
        N, N = W.shape

        # COMPUTE THE DEGREE (OMIT OR NOT SELF LOOPS)
        degree = W.sum(axis=0)
        #degree = W.sum(axis=0) - W.diagonal()

        # CHOOSE THE ORDER IN WHICH VERTICES WILL BE VISTED AT THE NEXT PASS
        #[~, rid]=sort(ss);     # arthur strategy
        #[~, rid]=sort(supernode_size);    #  thomas strategy
        #rid=randperm(N);                  #  metis/graclus strategy
        ss = np.array(W.sum(axis=0)).squeeze()
        rid = np.argsort(ss)

    return graphs, parents


# Coarsen a graph given by rr,cc,vv.  rr is assumed to be ordered
def metis_one_level(rr,cc,vv,rid,weights):

    nnz = rr.shape[0]
    N = rr[nnz-1] + 1

    marked = np.zeros(N, np.bool)
    rowstart = np.zeros(N, np.int32)
    rowlength = np.zeros(N, np.int32)
    cluster_id = np.zeros(N, np.int32)

    oldval = rr[0]
    count = 0
    clustercount = 0

    for ii in range(nnz):
        rowlength[count] = rowlength[count] + 1
        if rr[ii] > oldval:
            oldval = rr[ii]
            rowstart[count+1] = ii
            count = count + 1

    for ii in range(N):
        tid = rid[ii]
        if not marked[tid]:
            wmax = 0.0
            rs = rowstart[tid]
            marked[tid] = True
            bestneighbor = -1
            for jj in range(rowlength[tid]):
                nid = cc[rs+jj]
                if marked[nid]:
                    tval = 0.0
                else:
                    tval = vv[rs+jj] * (1.0/weights[tid] + 1.0/weights[nid])
                if tval > wmax:
                    wmax = tval
                    bestneighbor = nid

            cluster_id[tid] = clustercount

            if bestneighbor > -1:
                cluster_id[bestneighbor] = clustercount
                marked[bestneighbor] = True

            clustercount += 1

    return cluster_id


# In[4]:


graphs, parents = metis(sparse_adj, levels=2)


# ### 3.2) Graph - Permutation ?

# In[5]:


def compute_perm(parents):
    #parent contains the cluster_id vector of the parent graph
    indices = []
    if len(parents) > 0:
        #number of clusters of the last layer-graph
        M_last = max(parents[-1]) +1
        indices.append(list(range(M_last)))
    
    #iterate over the parents starting from the last one
    for parent in parents[::-1]:
        print("parent: {}".format(parent))
        
        pool_singeltons = len(parent)
        
        indices_layer = []
        #iterate over the cluster ids of the graphs starting from the coasest
        for i in indices[-1]:
            #find nodes of the same cluster i
            indices_node = list(np.where(parent == i)[0])
            assert 0<= len(indices_node) <= 2
            print("indices_node: {}".format(indices_node))
            
            #handle clusters with one node only
            if len(indices_node) is 1:
                indices_node.append(pool_singeltons) 
                pool_singeltons += 1
                print('new singelton: {}'.format(indices_node))
            # Add two nodes as children of a singelton in the parent.
            elif len(indices_node) is 0:
                indices_node.append(pool_singeltons+0)
                indices_node.append(pool_singeltons+1)
                pool_singeltons += 2
                print('new singelton(+2): {}'.format(indices_node))
                
            indices_layer.extend(indices_node)
            print("indices_layer: {}".format(indices_layer))
        
        indices.append(indices_layer)
    
    return indices[::-1]


# In[6]:


#ndarray such each array: the two consecutive nodes corrspond to a cluster.
perms = compute_perm(parents)


# In[7]:


def perm_adjacency(A, indices):
    if indices is None:
        return A
    
    M, M = A.shape
    Mnew = len(indices) #to evtl. add the fake nodes
    assert Mnew >= M
    A = A.tocoo() #triplet form of the matrix (rr,cc,vv)
    if Mnew > M:
        rows = scp.coo_matrix((Mnew-M, M), dtype=np.float32) #the to be added rows
        cols = scp.coo_matrix((Mnew, Mnew-M), dtype=np.float32)
        #add the isolated (0 weighted incident edges)
        A = scp.vstack([A, rows])
        A = scp.hstack([A, cols])
        
        #permute the rows and the columns according to indices:
        perm = np.argsort(indices)
        A.row = np.array(perm)[A.row]
        A.col = np.array(perm)[A.col]
    
    return A


# In[8]:


for i, A in enumerate(graphs):
    M, M = A.shape
    
    if not self_connections:
        A = A.tocoo()
        A.setdiag(0)
        
        if i < levels:
            A = perm_adjacency(A, perms[i])
            
        A = A.tocsr()
        A.eliminate_zeros()
        graphs[i] = A #change the matrices
        
        Mnew, Mnew = A.shape
        print('Layer {0}: M_{0} = |V| = {1} nodes ({2} added),'
              '|E| = {3} edges'.format(i, Mnew, Mnew-M, A.nnz//2))
    


# In[9]:


parents


# In[10]:


graphs


# In[11]:


for i in range(3):
    print(graphs[i].toarray())
    if i<2:
        print(parents[i])
        


# In[12]:


import networkx as nx


# In[13]:


import matplotlib.pyplot as plt
for i in [0,1,2]:
    plt.figure(figsize=[10,2])
    s = 131 + i
    plt.subplot(s)
    gra = nx.from_numpy_matrix(graphs[i].toarray())
    nx.draw_networkx(gra, with_labels=True, node_size=100) 
#plt.show()


# # Final plots

# In[14]:


import importlib
from lib import plotter as pl
importlib.reload(pl)


# In[16]:


pl.plot_coarse(6, graph_adj=adjacency_matrix, graph_adj2=graphs[1].toarray(), graph_adj3=graphs[2].toarray())

