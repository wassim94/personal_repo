
# coding: utf-8

# # S-MNIST main script
# 
# ## 0. Problem:
# * Standard MNIST classification problem solved by **Classical CNNs**
# * Instead of using the full grid, we sample it --> **SMNIST**
# * Consider it a graph -> less data, same/better performance
# 
# ### Libraries:
# This implementation is based on **Tensorflow**, **scipy** and **numpy** (add versions). These are called inside the scripts under ./lib/*
# 
# ## 0. Model parameters

# In[1]:


global prob #sampling probability
prob = 1e-1
global grid_size
grid_size = 28
global coarsening_levels
coarsening_levels = 4


# ## 1. Data Import & Preperation
# We import the MNIST dataset using **Tensorflow**. For now, we just arrange it in a data matrix (n x d)-matrix **X**, n being the number of samples, d number of features and a n-dimensional label vector **y** in (0, ..., C).
# 
# ### 1.1. Data Import

# In[2]:


import importlib
from lib import preprocessing as prep
importlib.reload(prep)


# In[3]:


[[train_data, val_data, test_data], [train_labels, val_labels, test_labels]] = prep.mnist_import()
[n_train, n_val, n_test] = [x.shape[0] for x in [train_data, val_data, test_data]]
prep.mnist_summary(train_data, val_data, test_labels, explain)


# ### 1.2. Graph Generation
# Now that we have imported the regular MNIST dataset, we define the so-called feature graph, on which the MNIST digits are to be defined as signals on a graph.
# 
# 
# 
# * We first create A, the adjacency matrix of the regular (full) grid, Ad being the dense matrix.
# * We then **sample** the grid by randomly replacing edges in **A**.

# In[4]:


import numpy as np
from lib import graph as gr
importlib.reload(gr)

z, sz, A, sA, Ad, sAd, mask = prep.grid_graph_2(grid_size, corners=True, seed=3, explain=explain, p=0.7)

print("number of edges of A: ", A.count_nonzero())
print("number of edges of the sampeled A: ", sA.count_nonzero())
print("we kept about", np.round(sA.count_nonzero()/A.count_nonzero() *100, 1), "% of the number of features.")

from lib import plotter as pl
importlib.reload(pl)

pl.draw_connected_grid(sAd, sz, Ad=Ad, z=z)

sample = 2
pl.plot_grids(z, sz)
pl.plot_data(train_data, sample, z, sz, mask)


# In[5]:


print(len(sAd))


# We have now imported the standard MNIST dataset, defined a regular (full) grid and sampeled it. We will generate the rest of the graphs: 
# These are **new** graphs that emerge from recursively coarsening the original graph. We note again that these are fixed and we can compute their Graph Laplacian matrices before defining signals on them.
# 
# ## 2. Graph Laplacians
# ### 2.1. All model graphs: graph coarsening using Graclus algorithm:
# *bold stands for stuff copied from original work*:
# **To be able to pool graph signals, we need first to coarsen the graph, i.e. to find which vertices to group together.** Graclus is a multilevel graph clustering algorithm. It indicates which vertices are to be grouped (i.e. features to be reduced).
# **The parameter here is the number of times to coarsen the graph. Each coarsening approximately reduces the size of the graph by a factor two.**

# In[ ]:


from lib import coarsening as co
importlib.reload(co)
importlib.reload(pl)
importlib.reload(prep)


# In[7]:


#example coarsening
graphs_eg, perm_eg = co.coarsen(sAeg, levels=coarsening_levels, self_connections=False)


# In[8]:


graphs, perm = co.coarsen(sA, levels=coarsening_levels, self_connections=False)


# ### Depending on the permutations of the grid, we rearrange the input data (train, val and test) to match the altered grid.
# of course, this does not affect the data labels.

# In[9]:


#for viz
cropped_data = np.zeros_like(train_data) #-1#00
cropped_data.T[mask,:] = np.copy(train_data.T[mask,:])
print(np.shape(cropped_data))

cropped_data_n = co.perm_data(cropped_data, perm)

def perform_perm(input_data, perm):
    cropped_data = np.zeros_like(input_data)
    cropped_data.T[mask,:] = np.copy(input_data.T[mask,:])
    cropped_data_n = co.perm_data(cropped_data, perm)
    return cropped_data_n

train_data = perform_perm(train_data, perm)
val_data = perform_perm(val_data, perm)
test_data = perform_perm(test_data, perm)



# In[10]:


import matplotlib.pyplot as plt

plt.figure(figsize=[10,5])
plt.subplot(121)
te = plt.scatter(z[:,0], -z[:,1], s=20, c=train_data[2], cmap='rainbow')#, vmax=0.25, vmin = -0.25)
plt.colorbar(te)
plt.subplot(122)
te = plt.scatter(z[:,0], -z[:,1], s=20, c=cropped_data[2], cmap='rainbow')#, vmax=90, vmin = 110)
plt.colorbar(te)
plt.show()


# ### 2.2. Graph Laplacian computation:
# for every graph out of the **graphs** list, we computed the **unnormalized** Laplacian that was introduced in the thesis and we plot its spectrum.

# In[11]:


L = [gr.laplacian(A, normalized=False) for A in graphs]
gr.plot_spectrum(L)


# ## 3. Spectral Convolutional Neural Network (SCNN)
# we now build the neural network using the theory introduced in the thesis.
# 
# ### 3.1. Network parameters
# The following parameters are mostly taken from the original work. We added the k_cut parameter that indicates which portion to take from the eigenvectors of the Laplacian

# In[12]:


params = dict()
params['dir_name']       = 'smnist-implementation_full/'
params['num_epochs']     = 10
params['batch_size']     = 100
params['eval_frequency'] = 30 * params['num_epochs']

# Building blocks.
params['brelu']          = 'b1relu'
params['pool']           = 'mpool1'

# Number of classes.
C = max(train_labels) + 1

# Architecture.
params['F']              = [32, 64] #[32, 64]  # Number of graph convolutional filters.
params['p']              = [4, 4]    # Pooling sizes.
params['M']              = [128, C]#[512, C]  # Output dimensionality of fully connected layers.

# Optimization.
params['regularization'] = 5e-4
params['dropout']        = 0.5       # p is probability to take the node (1-p to drop it.)
params['learning_rate']  = 0.02      # 0.03 in the paper but sgconv_sgconv_fc_softmax has difficulty to converge
params['decay_rate']     = 0.95
params['momentum']       = 0.9
params['decay_steps']    = n_train / params['batch_size']

#SG-CNN -> Pooling -> SG-CNN -> Pooling -> Fully-Connected -> Softmax parameters
name = 'fgconv_fgconv_fc_softmax'
params['dir_name'] += name
params['filter'] = 'fourier'
params['k_cut'] = 1
params['K'] = [L[0].shape[0], L[2].shape[0]]


# ### 3.2. Initialize model
# We use our model which is an instance of the model_perf class. It has a test function to test its performance with the above defined parameters

# In[13]:


from lib import utils as ut
importlib.reload(ut)

model_perf = ut.model_perf()


# In[14]:


from lib import models as mo
importlib.reload(mo)
cgnn_model = mo.cgcnn(L, **params)


# In[15]:


import time
start_1 = time.time()
model_perf.test(cgnn_model, name, params,
                    train_data, train_labels, val_data, val_labels, test_data, test_labels)             
end_2 = time.time()


# # non-trained vs. trained filters 

# In[18]:


import tensorflow as tf
sess = cgnn_model._get_session()
#var = sess.run(cgnn_model.weights_track['ihopeitworksnow_b'])
var = cgnn_model.weights_track['cl1']
var = np.array(var)
print(np.shape(var))

filters = [0, 5, 10, 30]
num_filters = len(filters)
figs = num_filters

x = dict()

for filt in filters:
    x[filt] = list()
    x[filt].append(np.squeeze(var[0,:,filt,0]))
    x[filt].append(np.squeeze(var[-1,:,filt,0]))
    x[filt].append(np.linalg.norm(x[filt][1]-x[filt][0]))
    print("norm of difference vector for gamma_{} = {}".format(filt,x[filt][-1]))
    
    #plt.subfigure()
    plt.hist([x[filt][0],x[filt][1]], bins=250, histtype='step')#, stacked=True)#, density=True)
    plt.legend(["initialization","step: {}".format(20*len(var))])
    plt.show()
    
var = cgnn_model.weights_track['cl1']
var = np.array(var)
print(np.shape(var))

x = dict()

for filt in filters:
    x[filt] = list()
    x[filt].append(np.squeeze(var[0,:,filt,0]))
    x[filt].append(np.squeeze(var[-1,:,filt,0]))
    x[filt].append(np.linalg.norm(x[filt][1]-x[filt][0]))
    print("norm of difference vector for gamma_{} = {}".format(filt,x[filt][-1]))
    
    #plt.subfigure()
    plt.hist([x[filt][0],x[filt][1]], bins=250, histtype='step')#, stacked=True)#, density=True)
    plt.legend(["initialization","step: {}".format(20*len(var))])
    plt.show()


sess.close()


# In[20]:


import matplotlib.pyplot as plt
#print(np.shape(x[:,0]))

subplots_n = (20 + num_filters)*10
sub_n = 1
for el in x.keys():
    d = x[el]
    plt.figure(figsize=[22,10])
    plt.subplot(subplots_n + sub_n)
    plt.title("untrained gamma_{}".format(el))  
    te = plt.scatter(z[:,0], -z[:,1], s=20, c=d[0], vmax=0.25, vmin = -0.25)
    plt.colorbar(te)
    sub_n += 1
    plt.subplot(subplots_n + sub_n)
    plt.title("trained gamma_{}".format(el))  
    te = plt.scatter(z[:,0], -z[:,1], s=20, c=d[1], vmax=0.25, vmin = -0.25)
    plt.colorbar(te)
    sub_n += 1
        
plt.show()


# ## Approximation of the basis with k<728 eigenvectors. 
# In this part, we train the same network using only half of the eigenvectors.

# In[21]:


params = dict()
params['dir_name']       = 'smnist-implementation_half/'
params['num_epochs']     = 10
params['batch_size']     = 100
params['eval_frequency'] = 30 * params['num_epochs']

# Building blocks.
params['brelu']          = 'b1relu'
params['pool']           = 'mpool1'

# Number of classes.
C = max(train_labels) + 1

# Architecture.
params['F']              = [32, 64]  # Number of graph convolutional filters.
#params['K']              = [25, 25]  # Polynomial orders.
params['p']              = [4, 4]    # Pooling sizes.
params['M']              = [512, C]  # Output dimensionality of fully connected layers.

# Optimization.
params['regularization'] = 5e-4
params['dropout']        = 0.5       # p is probability to take the node (1-p to drop it.)
params['learning_rate']  = 0.02      # 0.03 in the paper but sgconv_sgconv_fc_softmax has difficulty to converge
params['decay_rate']     = 0.95
params['momentum']       = 0.9
params['decay_steps']    = n_train / params['batch_size']

#SG-CNN -> Pooling -> SG-CNN -> Pooling -> Fully-Connected -> Softmax parameters
name = 'fgconv_fgconv_fc_softmax'
params['dir_name'] += name
params['filter'] = 'fourier_cut'
params['k_cut'] = 0.5
params['K'] = [L[0].shape[0], L[2].shape[0]]


# In[22]:


from lib import models as mo
importlib.reload(mo)
cgnn_model = mo.cgcnn(L, **params)


# In[23]:


start_2 = time.time()
model_perf.test(cgnn_model, name, params,
                    train_data, train_labels, val_data, val_labels, test_data, test_labels)             
end_2 = time.time()

