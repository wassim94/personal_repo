
# coding: utf-8

# # Eigenvectors of the Laplacian on graphs
# ## own contribution + libraries documentations

# In[1]:


from pygsp import graphs
import numpy as np
import matplotlib.pyplot as plt


# In[2]:


def basis_stats(spectrum, fourier_basis):
    dim = len(spectrum)
    def one_vec_stats(v, message):
        mean = np.round(np.mean(v),4)
        var = np.round(np.var(v), 4)
        message += "mean = {} and variance = {}".format(mean, var)
        print(message)
    one_vec_stats(fourier_basis[0], message="vector {}:".format(0))
    for i in range(1,11):
        idx = int((dim-1)/(11-i))
        one_vec_stats(fourier_basis[idx], message="vector {}:".format(idx))
        


# In[3]:


G = graphs.Minnesota()
G.plot()
plt.show()

G.compute_fourier_basis()
fourier_basis_rows = G.U
spectrum = G.e


fourier_basis_cols = fourier_basis_rows.transpose()


# In[4]:


plt.figure(figsize=(10,5))
plt.scatter(range(len(spectrum)), spectrum, marker='o', s=50)
plt.title('Spectrum of the Graph Laplacian of Minnesota road graph')
plt.show()


# In[5]:


basis_stats(fourier_basis=fourier_basis_cols, spectrum=spectrum)


# In[6]:


import matplotlib
matplotlib.rcParams.update({'font.size': 20})
import importlib
importlib.reload(plt)

fig, axes = plt.subplots(1, 2)
li = [[-0.025, 0.025],[-0.025, 0.025],[-0.025, 0.025]]

for j, i in enumerate([0, 1]):#, 2]):
    axes[j].figure.set_size_inches((16,4))
    G.plot_signal(limits=li[j],signal=fourier_basis_cols[i], ax=axes[j], plot_name= r"$\phi_{}$: node values for $\lambda_{}$".format(i,i))#, ax = axes[j])#, vertex_size=10)
    plt.set_cmap(plt.cm.coolwarm)
    axes[j].axis('off')
    plt.savefig("eigenvectors_1")


fig, axes = plt.subplots(1, 2)
for j, i in enumerate([9, 10]):
    str_i = "{" + str(i) + "}"
    axes[j].figure.set_size_inches((16,4))
    G.plot_signal(limits=li[j],signal=fourier_basis_cols[i], ax=axes[j], plot_name= r"$\phi_{}$: node values for $\lambda_{}$".format(str_i,str_i))#, ax = axes[j])#, vertex_size=10)
    plt.set_cmap(plt.cm.coolwarm)
    axes[j].axis('off')
    plt.savefig("eigenvectors_3")
    
    
plt.show()


# In[7]:


plt.imshow(fourier_basis_rows[:,100:200], cmap=plt.cm.ocean)
plt.colorbar()
plt.title("Fourier basis - Eigenvectors as columns")
plt.show()


# In[8]:


import matplotlib
matplotlib.rcParams.update({'font.size': 20})
import importlib
importlib.reload(plt)

plt.figure(figsize=(20,10))
plt.title("values on nodes")
for j,i in enumerate([0,1,9, 10]):
    idx = 221 + j
    plt.subplot(idx)
    plt.plot(range(1,len(fourier_basis_cols[0])+1), fourier_basis_cols[i], label="eigenvector u_{}".format(i))
    plt.legend(loc=1)
    plt.ylim([-0.1,0.1])
plt.savefig("ev")
plt.show()


# In[9]:


np.linalg.norm(fourier_basis_cols[0] - fourier_basis_cols[0][0])

