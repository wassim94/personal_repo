

#Whole file is own contribution

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import scipy

def plot_grids(z, sz, only_sampled=False):
    """plots original full grid and sampeled grid"""
    if not only_sampled:
        plt.figure(figsize=(10,5))
        plt.subplot(1,2,1)
        plt.scatter(z[:,0], -z[:,1], s=20)
        plt.subplot(1,2,2)
        plt.scatter(sz[:,0], -sz[:,1], s=20)
        plt.show()
    else:
        plt.figure(figsize=(10,5))
        plt.scatter(sz[:,0], -sz[:,1], s=20)
        plt.show()
        
        
def plot(data_matrix, data_point, z, mask, masked=True):
    data_matrix = data_matrix.T
    if masked: data_matrix = data_matrix[mask,:]
    M, N = data_matrix.shape
    m = int(np.sqrt(M))
    x = data_matrix[:,data_point]
    plt.scatter(z[:,0], -z[:,1], s=20, c=x+127.5)
    #plt.show()

def plot_data(data_matrix, x1, z, sz, mask):
    plt.figure(figsize=(10,5))
    plt.subplot(1,2,1)
    plot(data_matrix, x1, z, mask, masked=False)
    #plt.figure(figsize=(10,5))
    plt.subplot(1,2,2)
    plot(data_matrix, x1, sz, mask)
    plt.savefig("four.png")
    plt.show()
    
def edge_list(nodelist, m, z):
    def map_point(m, node):
        x = (node // m)
        y = (node % m)
        return (x,y)
    
    def dist(i,j,z):
        z = [map_point(m ,p) for p in [i,j]]
        d = scipy.spatial.distance.pdist(z, metric='euclidean')
        return d
    
    edgelist = []
    edgedist = []
    for i in nodelist:
        for j in nodelist:
            if i==j: pass 
            elif dist(i,j,z) > np.sqrt(2): pass
            else:
                edgelist.append((i,j))
                #edgedist[(i,j)]=np.round(dist(i,j,z),1)
                edgedist.append(dist(i,j,z).squeeze())
    
    return edgelist,edgedist

def draw_connected_grid(sAd, sz, mask=None, Ad=None, z=None, only_sampled=False):
    if only_sampled:
        plt.figure(figsize=(2.5,1.25))
        Graph_1 =nx.from_numpy_matrix(Ad)
        nodelist=list(mask)
        edgelist, edgedistance=edge_list(nodelist, np.sqrt(len(z)), z)
        nx.draw(Graph_1, node_size=150, pos=z, node_color='grey', with_labels=True, nodelist=nodelist, edgelist=edgelist, edge_color=edgedistance, width=2)#, with_labels=True)
        print("The feature graph is not a knn graph. It is a <sqrt(2) graph.")
    else:
        plt.figure(figsize=(10,5))
        plt.subplot(1,2,1)
        Graph_1=nx.from_numpy_matrix(Ad)
        nx.draw(Graph_1, pos=np.flip(z,1), node_size=15)
        plt.subplot(1,2,2)
        Graph_2=nx.from_numpy_matrix(sAd)
        nx.draw(Graph_2, pos=sz, node_size=15, )
        plt.gca().invert_yaxis()
        plt.savefig("grid.png")
    plt.show()
    
def return_edgecol(A, G):
    
    edgecolor=list()
    for edge in list(G.edges()):
        x, y=edge
        edgecolor.append(A[x,y])

    return edgecolor

def draw_connected_grid_example(sAd, sz):
    
    plt.figure(figsize=(2.5,1.25))
    Graph_1 =nx.from_numpy_matrix(sAd)

    nx.draw(Graph_1, node_size=150, pos=sz, node_color='grey', with_labels=True, edge_color=return_edgecol(sAd, Graph_1), width=2)
    plt.show()

    
from matplotlib.patches import Ellipse

def plot_coarse(grid_size, graph_adj, graph_adj2, graph_adj3):
    
    def tmp_fn(m=grid_size, number_edges=4, metric='euclidean', seed=3):
        from . import graph
        z = graph.grid(m)
        np.random.seed(seed)        
        A = graph.sklearn_adjacency(z, k=number_edges, metric=metric)
        
        import scipy.sparse
        A = A.toarray()
        A[A > A.max()/1.5] = 0 #modified < to >
        A = scipy.sparse.csr_matrix(A)
        #print('{} edges'.format(A.nnz))
        dist, idx = graph.distance_scipy_spatial(z, k=number_edges, metric=metric)

        Ad = A.toarray()

        return z, A, Ad
       
    z, A, Ad = tmp_fn()
    plt.figure(figsize=(10,5))
    Graph=nx.from_numpy_matrix(Ad)
    
    def draw_first_g():
        ##FIRST GRAPH###
        
        Graph_1=nx.from_numpy_matrix(graph_adj)
        sz = [[1/5,2/5],
              [2/5, 3/5],
              [4/5, 3/5],
              [4/5, 1/5],
              [2/5,1/5]
             ]
        edge_w = dict()
        for i in Graph_1.edges():
            edge_w[i]=1
        print(edge_w)
        
        a = plt.subplot(131)
        nx.draw(Graph, pos=np.flip(z,1), node_size=0, width = 0.5)
        nx.draw(Graph_1, node_size=350, pos=sz, node_color='grey', with_labels=True, width=3)
        nx.draw_networkx_edge_labels(Graph_1, edge_labels=edge_w, pos=sz)
        #ellipses
        deltas = [0,-45]
        coors = [(3/5, 3/5), (3/10, 3/10)]
        
        ells = [Ellipse(coor, 0.6, 0.3, delta) for coor, delta in zip(coors,deltas)]
        ells.append(Ellipse((4/5, 1/5), 0.2,0.2, 0))
        
        for e in ells:
            e.set_clip_box(a.bbox)
            e.set_alpha(0.1)
            a.add_artist(e)
        
        ##SECOND GRAPH###
        
        Graph_2=nx.from_numpy_matrix(graph_adj2)
        sz = [[3/5, 3/5],
              [3/10, 3/10],
              [4/5, 1/5]
             ]
        edge_w = dict()
        edge_w[(0,1)]=1
        edge_w[(1,2)]=2
        edge_w[(0,2)]=1
        a = plt.subplot(132)
        nx.draw(Graph, pos=np.flip(z,1), node_size=0, width = 0.5)
        nx.draw(Graph_2, node_size=350, pos=sz, node_color='grey', with_labels=True, width=3)
        nx.draw_networkx_edge_labels(Graph_2, edge_labels=edge_w, pos=sz)
        
        #ellipses
        delta = -60
        coor = (7/10, 4/10)
        
        ells = [Ellipse(coor, 0.6, 0.3, delta, color='red')]
        ells.append(Ellipse((3/10, 3/10), 0.2,0.2, 0, color='red'))
        
        for e in ells:
            e.set_clip_box(a.bbox)
            e.set_alpha(0.1)
            a.add_artist(e)
        
        
        ##THIRD GRAPH###
        
        Graph_3=nx.from_numpy_matrix(graph_adj3)
        sz = [[3/10, 3/10],
              [7/10, 4/10]
             ]
        edge_w = {(0,1):3}
        plt.subplot(133)
        nx.draw(Graph, pos=np.flip(z,1), node_size=0, width = 0.5)
        nx.draw(Graph_3, node_size=350, pos=sz, node_color='grey', with_labels=True, width=3)
        nx.draw_networkx_edge_labels(Graph_3, edge_labels=edge_w, pos=sz)
        
        #edge cut
        #plt.plot([3/5+0.01, 2/5+0.01], [1/5+0.01, 1/2+0.01], 'k-')

    plt.figure(figsize= [15,4])
    draw_first_g()
    
    def draw_second_g():
        ###FIRST GRAPH###
        
        Graph_3=nx.from_numpy_matrix(graph_adj3)
        sz = [[3/10, 3/10],
              [7/10, 4/10]
             ]
        edge_w = {(0,1):3}
        plt.subplot(133)
        nx.draw(Graph, pos=np.flip(z,1), node_size=0, width = 0.5)
        nx.draw(Graph_3, node_size=350, pos=sz, node_color='grey', with_labels=True, width=3)
        nx.draw_networkx_edge_labels(Graph_3, edge_labels=edge_w, pos=sz)
    
    
    plt.savefig("coarsening_one.jpg")
    plt.show()
