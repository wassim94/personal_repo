import sklearn.metrics
import sklearn.neighbors
import matplotlib.pyplot as plt
import scipy.sparse
import scipy.sparse.linalg
import scipy.spatial.distance
import numpy as np


explain = False

def grid(m, dtype=np.float32):
    """Return the embedding of a grid graph.
    my explanation:
        *m: cardinality of points on x- and y-axies
        *creates a vector z with (m^2x2) dimensions such as the grid [0,1]^2 contains m^2 points"""
    M = m**2
    x = np.linspace(0, 1, m, dtype=dtype)
    y = np.linspace(0, 1, m, dtype=dtype)
    xx, yy = np.meshgrid(x, y)
    z = np.empty((M, 2), dtype)
    z[:, 0] = xx.reshape(M)
    z[:, 1] = yy.reshape(M)
    return z

#comments are own contribution
def distance_scipy_spatial(z, k=4, metric='euclidean'):
    """Compute exact pairwise distances."""
    #compute euclidean distance between vectors of same dimensions stored in z[i]
    d = scipy.spatial.distance.pdist(z, metric)
    #transform distance vector d to distance matrix of len(z) x len(z): 
    d = scipy.spatial.distance.squareform(d)
    # k-NN graph: gives the indices of the closest point on a graph based on the euclidean distance
    idx = np.argsort(d)[:, 1:k+1]
    d.sort()
    #store distances of closest points
    d = d[:, 1:k+1]
    return d, idx

def distance_sklearn_metrics(z, k=4, metric='euclidean'):
    """Compute exact pairwise distances."""
    d = sklearn.metrics.pairwise.pairwise_distances(
            z, metric=metric, n_jobs=-2)
    # k-NN graph.
    idx = np.argsort(d)[:, 1:k+1]
    d.sort()
    d = d[:, 1:k+1]
    return d, idx

def distance_lshforest(z, k=4, metric='cosine'):
    """Return an approximation of the k-nearest cosine distances."""
    assert metric is 'cosine'
    lshf = sklearn.neighbors.LSHForest()
    lshf.fit(z)
    dist, idx = lshf.kneighbors(z, n_neighbors=k+1)
    assert dist.min() < 1e-10
    dist[dist < 0] = 0
    return dist, idx


def adjacency(dist, idx):
    """Return the adjacency matrix of a kNN graph.
    my explanation:
        weights are assigned to the closest points on the grid/on the graph!"""
    M, k = dist.shape
    assert M, k == idx.shape
    assert dist.min() >= 0

    # Weights.
    sigma2 = np.mean(dist[:, -1])**2
    dist = np.exp(- dist**2 / sigma2)

    # Weight matrix.
    I = np.arange(0, M).repeat(k)
    J = idx.reshape(M*k)
    V = dist.reshape(M*k)
    W = scipy.sparse.coo_matrix((V, (I, J)), shape=(M, M))

    # No self-connections.
    W.setdiag(0)

    # Non-directed graph.
    bigger = W.T > W
    W = W - W.multiply(bigger) + W.T.multiply(bigger)

    assert W.nnz % 2 == 0
    assert np.abs(W - W.T).mean() < 1e-10
    assert type(W) is scipy.sparse.csr.csr_matrix
    return W

def sklearn_adjacency(z, k=4, metric='euclidean'):
    A = sklearn.neighbors.kneighbors_graph(z, k, mode='distance', include_self=False, metric=metric)
    return A
    
#explanatory function    
def recall_what_adjacency_does(see_neighbors_of=0, neighbors=4, verbose=True):
    if verbose: print("1) first create grid variable as graph using 'grid' function")
    grid_ = grid(10)
    if verbose: print("2) once grid created... create distance matrix and indices using one of the distance functions")
    d, idx = distance_scipy_spatial(grid_)
    if verbose: print("3) now we create the adjacency matrix W and turn it to dense")
    W = adjacency(d, idx)
    W_dense = W.todense()
    if verbose: print("4) now we can see which nodes are the closest to the chosen node: " + str(see_neighbors_of))
    k_neighbors = []
    for i in range(100):
        if(W_dense[see_neighbors_of,i]): 
            k_neighbors.append([W_dense[see_neighbors_of,i],i])
    k_neighbors.sort()
    print("--> the k closest points are: \n" + str(k_neighbors))
    return grid_, d, idx, W


if explain: grid_, d, idx, W = recall_what_adjacency_does()

#taken from graph.py but with minor changes (value of nodes)
def replace_random_edges(A, noise_level):
    """Replace randomly chosen edges by random edges."""
    M, M = A.shape
    n = int(noise_level * A.nnz // 2)

    indices = np.random.permutation(A.nnz//2)[:n]
    rows = np.random.randint(0, M, n)
    cols = np.random.randint(0, M, n)
    vals = np.random.uniform(0, 1, n)
    assert len(indices) == len(rows) == len(cols) == len(vals)

    A_coo = scipy.sparse.triu(A, format='coo')
    assert A_coo.nnz == A.nnz // 2
    assert A_coo.nnz >= n
    A = A.tolil()

    for idx, row, col, val in zip(indices, rows, cols, vals):
        old_row = A_coo.row[idx]
        old_col = A_coo.col[idx]

        A[old_row, old_col] = 0
        A[old_col, old_row] = 0
        A[row, col] = val #1?
        A[col, row] = val #1?

    A.setdiag(0)
    A = A.tocsr()
    A.eliminate_zeros()
    return A

#own contribution to account for changes between the grid and the graphs
def compare_grids(A, sA, explain):
    Ad = A.toarray()
    sAd = sA.toarray()
    if not explain: return Ad, sAd
    
    bool_1 = (Ad - sAd == Ad)
    bool_2 = (Ad > 0)
    to_zeros = (np.sum(bool_1 & bool_2)/(len(Ad)))*100
    print("{}% of grid edges omitted.".format(np.round(to_zeros, 1)))
    
    bool_1 = sAd > 0
    bool_2 = (Ad == 0)
    to_existant = (np.sum(bool_1 & bool_2)/(len(Ad)))*100
    print("{}% of grid edges activated.".format(np.round(to_existant, 1)))
    
    rel_diff = (np.sum(Ad != sAd)/(len(Ad)))*100
    to_altered = rel_diff - to_existant - to_zeros
    print("{}% of grid edges have had their weights changed.".format(np.round(to_altered, 1)))
    
    unchanged = len(Ad) - np.sum(Ad != sAd)
    unchanged *= (100/len(Ad))
    print("{}% of grid edges are unchanged.".format(np.round(unchanged, 1)))
    
    print("Full grid: {} active edges and {} inactive edges out of {} possible total edges (complete graph).".format(np.sum(Ad > 0), np.sum(Ad == 0), len(Ad)**2))
    print("Sampled grid: {} active edges and {} inactive edges out of {} possible total edges (complete graph).".format(np.sum(sAd > 0), np.sum(sAd == 0), len(sAd)**2))
    
    return Ad, sAd

def laplacian(W, normalized=True):
    """Return the Laplacian of the weigth matrix."""

    # Degree matrix.
    d = W.sum(axis=0)

    # Laplacian matrix.
    if not normalized:
        D = scipy.sparse.diags(d.A.squeeze(), 0)
        L = D - W
    else:
        d += np.spacing(np.array(0, W.dtype))
        d = 1 / np.sqrt(d)
        D = scipy.sparse.diags(d.A.squeeze(), 0)
        I = scipy.sparse.identity(d.size, dtype=W.dtype)
        L = I - D * W * D

    # assert np.abs(L - L.T).mean() < 1e-9
    assert type(L) is scipy.sparse.csr.csr_matrix
    return L

def lmax(L, normalized=True):
    """Upper-bound on the spectrum."""
    if normalized:
        return 2
    else:
        return scipy.sparse.linalg.eigsh(
                L, k=1, which='LM', return_eigenvectors=False)[0]


def fourier(L, algo='eigh', k=1):
    """Return the Fourier basis, i.e. the EVD of the Laplacian.
    my explanation:
        **uses different algorithms to compute the Bais of Eigenvectors (EVD)
        **algos containing 's' take k as argument to compute the first k EVs."""

    def sort(lamb, U):
        idx = lamb.argsort()
        return lamb[idx], U[:, idx]

    if algo is 'eig':
        lamb, U = np.linalg.eig(L.toarray())
        lamb, U = sort(lamb, U)
    elif algo is 'eigh':
        lamb, U = np.linalg.eigh(L.toarray())
    elif algo is 'eigs':
        lamb, U = scipy.sparse.linalg.eigs(L, k=k, which='SM')
        lamb, U = sort(lamb, U)
    elif algo is 'eigsh':
        lamb, U = scipy.sparse.linalg.eigsh(L, k=k, which='SM')

    return lamb, U

def rescale_L(L, lmax=2):
    """Rescale the Laplacian eigenvalues in [-1,1]."""
    M, M = L.shape
    I = scipy.sparse.identity(M, format='csr', dtype=L.dtype)
    L /= lmax / 2
    L -= I
    return L

def recall_what_fourier_does(verbose=True):
    if verbose: print("1) First compute L: the unnormalized Laplacian of W; a " +  str(W.get_shape()) + " matrix")
    L = laplacian(W, normalized=False)
    if verbose: print("2) now we compute the (complete) Basis of Eigenvectors stored in U.")
    lamb, U = fourier(L, algo='eigh')
    return L, lamb, U 


if explain: L, lamb, U = recall_what_fourier_does()



def rescale_L(L, lmax=2):
    """Rescale the Laplacian eigenvalues in [-1,1]."""
    M, M = L.shape
    I = scipy.sparse.identity(M, format='csr', dtype=L.dtype)
    L /= lmax / 2
    L -= I
    return L

def plot_spectrum(L, algo='eig'):
    """Plot the spectrum of a list of multi-scale Laplacians L."""
    # Algo is eig to be sure to get all eigenvalues.
    plt.figure(figsize=(17, 5))
    for i, lap in enumerate(L):
        lamb, U = fourier(lap, algo)
        step = 2**i
        x = range(step//2, L[0].shape[0], step)
        lb = 'L_{} spectrum in [{:1.2e}, {:1.2e}]'.format(i, lamb[0], lamb[-1])
        plt.plot(x, lamb, '.', label=lb)
    plt.legend(loc='best')
    plt.xlim(0, L[0].shape[0])
    plt.ylim(ymin=0)
    plt.show()



if explain: 
    print("unscaled spectrum")
    plot_spectrum([L])
    plt.show()
    print("unscaled spectrum")
    plot_spectrum([rescale_L(L)])
    plt.show()
