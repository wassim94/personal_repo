from tensorflow.examples.tutorials.mnist import input_data
import os
import numpy as np
import sys, os
sys.path.insert(0, '.')
from lib import models, graph, coarsening, utils

global number_edges
number_edges = 4
global metric
metric = 'euclidean'
global normalized_laplacian
normalized_laplacian = False
global coarsening_levels
coarsening_levels = 4
global dir_data
dir_data = os.path.join('.', 'data', 'mnist/')


#taken from mnist.ipynb
def mnist_import():
    """import mnist dataset using tensorflow.
    Data is splitted into train, validation and test data."""
    mnist = input_data.read_data_sets(dir_data, one_hot=False)
    train_data = mnist.train.images.astype(np.float32)
    val_data = mnist.validation.images.astype(np.float32)
    test_data = mnist.test.images.astype(np.float32)
    train_labels = mnist.train.labels
    val_labels = mnist.validation.labels
    test_labels = mnist.test.labels
    print("\n" + 10*'*' + " \n MNIST successfully imported and split into:")
    print("  -train/val/test data and train/val/test labels \n" + 10*'*')
    return [[train_data, val_data, test_data], [train_labels, val_labels, test_labels]]

#own contribution
def mnist_summary(x_train, x_val, y_test, explain=False):

	"""takes x_train, x_val, y_test as inputs and prints a summary about the data matrix X"""
	
	if not explain: return	
	
	training_samples = x_train.shape[0]
	number_features = x_train.shape[1]
	val_samples = x_val.shape[0]
	test_samples = y_test.shape[0]
	max_classes = y_test.max()

	print("number of training samples: " + str(training_samples))
	print("number of validation samples: " + str(val_samples))
	print("number of test samples: " + str(test_samples))
	print("number of features: " + str(number_features))
	print("classes: " + str(range(max_classes)))

    
#taken from mnist.ipynb with minor modifications (definition of edges)
def grid_graph(m, corners=False, explain=False):
    """my explanation:
        **1) create grid on [0,1]^2 with ALL m^2 data points
        **2) dist, idx: distance matrix and closest vertices ids
        **3) A: grid adjacency matrix
        **4) comparison not clear..."""
    z = graph.grid(m)
    dist, idx = graph.distance_scipy_spatial(z, k=number_edges, metric=metric)
    A = graph.sklearn_adjacency(z, k=number_edges, metric=metric)  


    # Connections are only vertical or horizontal on the grid.
    # Corner vertices are connected to 2 neightbors only.
    if corners:
        import scipy.sparse
        A = A.toarray()
        A[A > A.max()/1.5] = 0 #modified < to > #own contribution
        A = scipy.sparse.csr_matrix(A)
        print('{} edges'.format(A.nnz))
    Ad = A.toarray()

    return z, A

#own contribution
def symmetr(matrix):
    dense_matrix = matrix.toarray()
    assert(dense_matrix.shape[0] == dense_matrix.shape[1])
    dense_matrix = dense_matrix + dense_matrix.T
    dense_matrix /= 2
    matrix = matrix + matrix.T
    matrix /= 2
    return matrix, dense_matrix


#readaptation of grid_graph
def grid_graph_2(m, corners=False, explain=False, seed=1, p=0.8):
    """my explanation:
        **1) create grid on [0,1]^2 with ALL m^2 data points
        **2) dist, idx: distance matrix and closest vertices ids
        **3) A: grid adjacency matrix
        **4) comparison"""
    z = graph.grid(m)
    np.random.seed(seed)
    mask = np.arange(m**2)
    #shuffle then take the p*784 first ones
    np.random.shuffle(mask)
    mask = mask[:int(p*(m**2))]
    mask = sorted(mask)
    sz = z[mask,:]
    #sX = X[mask,:]
    dist, idx = graph.distance_scipy_spatial(z, k=number_edges, metric=metric)
    s_dist, s_idx = graph.distance_scipy_spatial(sz, k=number_edges, metric=metric)
    A = graph.sklearn_adjacency(z, k=number_edges, metric=metric)
    sA = graph.sklearn_adjacency(sz, k=number_edges, metric=metric)  


    # Connections are only vertical or horizontal on the grid.
    # Corner vertices are connected to 2 neightbors only.
    if corners:
        import scipy.sparse
        A = A.toarray()
        A[A > A.max()/1.5] = 0 #modified < to >
        A = scipy.sparse.csr_matrix(A)
        print('{} edges'.format(A.nnz))

        sA = sA.toarray()
        sA[sA > sA.max()/1.5] = 0 #modified < to >
        sA = scipy.sparse.csr_matrix(sA)
        print('{} edges'.format(sA.nnz))
    Ad = A.toarray()
    sAd = sA.toarray()

    if m==28: sA, sAd = symmetr(sA)
    return z, sz, A, sA, Ad, sAd, mask
