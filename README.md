# Welcome to my repo! Nice to have you here :)

This repo contains some of the work/scripts I did in the context of Machine Learning (mainly deep learning) 😁📊📚. 

I will keep updating it :) Hope you have fun 🍻! 


****

**Repo structure: 📐**
- ./Bachelor Thesis/: contains the whole work I submitted for my bachelor thesis at the TUM entitled: **Spectral Graph Theory in The Context of Convoultional Neural Networks**
- ./Personal Projects/: Currently contains only **parts of** one project I'm working on with a couple of friends: **Algorithmic Trading Using Deep Learning** (mainly Deep Reinforcement Learning)
- my_tutos.txt: approximative list of the Machine Learning fields in which I re-implemented several tutorials and online courses. I will update this with the new tutorials I work on.


****
**To my person: :angel: ️**

I am a Math student at the TUM (Technical University of Munich), currently enrolled at the _Mathematics in Data Science_ masters program.
``` 
My dream is to become the greatest data scientist the world has ever known. I want to be data science Guru, creating and also transferring 
knowledge to upcoming generations to help make the world a better place. I love math and I think math loves me back.
Of course I want to be rich too.
``` 
``` 
I love music, I love football (nope, not the american football 🤦‍♂️)️, I love hanging out with people, dancing, drinking, talking politics,
talking philosophy, talking history but also talking about girls, listening to cheesy music and surely eating. Italian is my favorite if you 
ever wondered.
``` 

****
**Contact Information: ☎**

- Email: boubaker.wa(at)hotmail.de
- Facebook: https://www.facebook.com/wassimigho
- LinkedIn: https://www.linkedin.com/in/wassim-boubaker/


